const express = require('express')
const app = (express())
const router = require("./router")
const port = process.env.PORT || 3000

app.use(express.json())
app.use(express.urlencoded({ extended: true}))

app.set("view engine", "ejs")
app.use(router)

app.listen(port, () => {
    console.log(`server ready on ${port}`)
})